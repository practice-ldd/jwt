package springcloud.service.jwt;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

/**
 *
 * @author liudong
 * @date 2018/11/06 17:59:53
 * @return
 */
@SpringBootApplication
//@EnableEurekaClient
@EnableFeignClients
@EnableHystrixDashboard
@EnableHystrix
@MapperScan(basePackages = {"springcloud.service.jwt.mapper"})
//@ComponentScan(basePackages = {"com.demo2do","com.suidifu", "com.zufangbao"})
public class ShiroJwtServerApplication {

    public static void main(String[] args) {

        SpringApplication.run(ShiroJwtServerApplication.class, args);
        System.out.println("启动完毕");

    }
}
