package springcloud.service.jwt.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springcloud.service.jwt.mapper.UserMapper;
import springcloud.service.jwt.model.ResultMap;

/**
 * @Auther: liudong
 * @Date: 18-11-2 15:03
 * @Description:
 */
@RestController
@RequestMapping("/admin")
public class AdminController {
    private final ResultMap resultMap;
    private final UserMapper userMapper;
    @Autowired
    public AdminController(UserMapper userMapper, ResultMap resultMap) {
        this.userMapper = userMapper;
        this.resultMap = resultMap;
    }

    @GetMapping(value = "/getMessage")
//    @RequiresRoles("admin")
    public ResultMap getMessage() {
        return resultMap.success().code(200).message("该接口不做角色限制，可以获得该接口的信息！");
    }



    @GetMapping("/getUser")
    @RequiresRoles("admin")
    public ResultMap getUser() {
        List<String> list = userMapper.getUser();
        return resultMap.success().code(200).message(list);
    }

    /**
     * 封号操作
     */
    @PostMapping("/banUser")
    @RequiresRoles("admin")
    public ResultMap updatePassword(String username) {
        userMapper.banUser(username);
        return resultMap.success().code(200).message("成功封号！");
    }

    /**
     * 只有admin  角色才能访问本类
     */
    @ModelAttribute
    @RequiresRoles("admin")
    public void initMethod(){
        System.out.println("admin权限认证");
    }
}