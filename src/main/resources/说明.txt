token.getUsername()  //获得用户名
String token.getPrincipal() //获得用户名
Object token.getPassword()  //获得密码
char[] token.getCredentials() //获得密码 Object

Filter 	解释
anon 	无参，开放权限，可以理解为匿名用户或游客
authc 	无参，需要认证
logout 	无参，注销，执行后会直接跳转到shiroFilterFactoryBean.setLoginUrl(); 设置的 url
authcBasic 	无参，表示 httpBasic 认证
user 	无参，表示必须存在用户，当登入操作时不做检查
ssl 	无参，表示安全的URL请求，协议为 https
perms[user] 	参数可写多个，表示需要某个或某些权限才能通过，多个参数时写 perms[“user, admin”]，当有多个参数时必须每个参数都通过才算通过
roles[admin] 	参数可写多个，表示是某个或某些角色才能通过，多个参数时写 roles[“admin，user”]，当有多个参数时必须每个参数都通过才算通过
rest[user] 	根据请求的方法，相当于 perms[user:method]，其中 method 为 post，get，delete 等
port[8081] 	当请求的URL端口不是8081时，跳转到schemal://serverName:8081?queryString 其中 schmal 是协议 http 或 https 等等，serverName 是你访问的 Host，8081 是 Port 端口，queryString 是你访问的 URL 里的 ? 后面的参数